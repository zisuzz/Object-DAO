# Object-DAO
这个JS类库将用来管理Indexed Database API、localStorage、Application Cache中的数据状态、数据的时间戳管理、定义本地存储库的容量大小（最大可以存储95MB数据、耗时在7s左右）未来希望这个组件能上传到NPM当中
得益于《JavaScript设计模式》
1. 管理localStorage中的数据状态（已完成70%）
2. 管理Indexed Database API中的数据状态（未开始)
3. 管理Application Cache中的数据状态（未开始）
4. 暂定